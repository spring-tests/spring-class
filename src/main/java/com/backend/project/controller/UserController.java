package com.backend.project.controller;

import com.backend.project.model.request.UserRequest;
import com.backend.project.model.response.UserResponse;
import com.backend.project.service.UserService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import static com.backend.project.Roles.ADMIN;

@RestController
@RequestMapping("/user")
public class UserController {
    public static final String ROLE_ADMIN = ADMIN;

    @Autowired
    UserService userService;

    @GetMapping("")
    @Secured({ROLE_ADMIN})
    public List<UserResponse> list () {
        return userService.getAll();
    }

    @GetMapping("/page")
    @Secured({ROLE_ADMIN})
    public Page<UserResponse> listinPage (
            @RequestParam("page") int page,
            @RequestParam("size") int size
    ) {
        return userService.getAllInPages(PageRequest.of(page, size));
    }

    @PostMapping("/save")
    @Secured({ROLE_ADMIN})
    public UserResponse save (@Valid @RequestBody UserRequest userRequest) {
        return userService.addNewUser(userRequest);
    }

    @PutMapping("/update/{userId}")
    @Secured({ROLE_ADMIN})
    public UserResponse update (
            @PathVariable("userId") Long userId,
            @Valid @RequestBody UserRequest userRequest) {
        return userService.updateUser(userId, userRequest);
    }

    @DeleteMapping("delete/{userId}")
    @Secured({ROLE_ADMIN})
    public UserResponse delete (
            @PathVariable("userId") Long userId
    ) {
        return userService.deleteUser(userId);
    }
}

package com.backend.project;

public interface Roles {
    String ADMIN = "ROLE_ADMIN";
    String USER = "ROLE_USER";
    String WATCHER = "ROLE_USER";
}

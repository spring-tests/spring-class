package com.backend.project.core;

import com.backend.project.tasks.AddAdminUserTask;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class DataInitializer {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private AddAdminUserTask addAdminUserTask;

    @Bean
    public Step step() {
        return this.stepBuilderFactory.get("Add if null").tasklet(addAdminUserTask).build();
    }

    @Bean
    public Job job() {
        return this.jobBuilderFactory.get("Add initial admin user").start(step()).build();
    }
}

package com.backend.project.feign.service;

import com.backend.project.feign.FeignBaseBuilderProvider;
import com.backend.project.feign.client.ScryfallClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class ScryfallService {

    @Autowired
    private FeignBaseBuilderProvider provider;

    @Cacheable(cacheNames = "cardCache")
    public String getCards() {
        return provider.getFeign(ScryfallClient.class).getCardsByPage();
    }
}
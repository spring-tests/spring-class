package com.backend.project.feign.controller;

import com.backend.project.feign.service.ScryfallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/scryfall")
public class ScryfallController {

    @Autowired
    private ScryfallService scryfallService;

    @GetMapping("/cards")
    public String getCardsByPage() {
        return scryfallService.getCards();
    }
}

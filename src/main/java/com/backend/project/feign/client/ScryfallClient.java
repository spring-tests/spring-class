package com.backend.project.feign.client;

import feign.RequestLine;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "scryfall-client", url = "${feign.url.base-url}")
public interface ScryfallClient {

    @RequestLine("GET /cards?pretty=true")
    String getCardsByPage();

}

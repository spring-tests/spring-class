package com.backend.project.feign;

import feign.Feign;
import feign.Logger;
import feign.slf4j.Slf4jLogger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class FeignBaseBuilderProvider {

    @Value("${feign.url.base-url}")
    private String BASE_URL;

    public <T> T getFeign(Class<T> apiType) {
        return getFeign(apiType, BASE_URL);
    }

    public <T> T getFeign(Class<T> apiType, String baseUrl) {
        log.info("getFeign(" + apiType + ", " + baseUrl + ")");
        return Feign.builder()
                .logger(new Slf4jLogger(apiType))
                .logLevel(Logger.Level.FULL)
                .target(apiType, baseUrl);
    }
}

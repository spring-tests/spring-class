package com.backend.project.converter;

import com.backend.project.model.entity.UserEntity;
import com.backend.project.model.request.UserRequest;
import com.backend.project.model.response.UserResponse;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserConverter {

    @Autowired
    PasswordEncoder passwordEncoder;

    public UserResponse getUserResponse (UserEntity userEntity) {
        return new UserResponse(userEntity.getId(), userEntity.getName(), userEntity.getEmail(), userEntity.getRole());
    }

    public List<UserResponse> getUserResponses (List<UserEntity> userEntities) {
        return userEntities.stream()
                .map(userEntity -> new UserResponse(userEntity.getId(), userEntity.getName(), userEntity.getEmail(), userEntity.getRole()))
                .collect(Collectors.toList());
    }

    public Page<UserResponse> findAllInPages (Page<UserEntity> page) {
        return page.map(userEntity -> new UserResponse(userEntity.getId(), userEntity.getName(), userEntity.getEmail(), userEntity.getRole()));
    }

    public UserEntity getUserEntity (UserRequest userRequest) {
        return new UserEntity(
                userRequest.getName(),
                userRequest.getEmail(),
                passwordEncoder.encode(userRequest.getPassword()),
                userRequest.getRole()
        );
    }
}

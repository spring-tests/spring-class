package com.backend.project.service;

import com.backend.project.converter.UserConverter;
import com.backend.project.model.request.UserRequest;
import com.backend.project.model.response.UserResponse;
import com.backend.project.repository.UserRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserConverter userConverter;

    public List<UserResponse> getAll () {
        return userConverter.getUserResponses(userRepository.findAll());
    }

    public Page<UserResponse> getAllInPages (PageRequest pageRequest) {
        return userConverter.findAllInPages(userRepository.findAll(pageRequest));
    }

    public UserResponse addNewUser (UserRequest userRequest) {
        return userConverter.getUserResponse(userRepository.save(userConverter.getUserEntity(userRequest)));
    }

    public UserResponse updateUser (Long userId, UserRequest userRequest) {
        return userRepository.findById(userId)
                .map(user -> {
                    user.setName(userRequest.getName());
                    user.setEmail(userRequest.getEmail());
                    user.setPassword(userRequest.getPassword());

                    return userConverter.getUserResponse(userRepository.save(user));
                })
                .orElseThrow(() -> new RuntimeException("User not founded"));
    }

    public UserResponse deleteUser (Long userId) {
        return userRepository.findById(userId)
                .map(user -> {
                    userRepository.delete(user);
                    return userConverter.getUserResponse(user);
                })
                .orElseThrow(() -> new RuntimeException("User not founded"));
    }
}

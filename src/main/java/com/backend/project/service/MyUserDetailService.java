package com.backend.project.service;

import com.backend.project.model.entity.UserEntity;
import com.backend.project.repository.UserRepository;
import java.util.Collection;
import java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername (String loginOrEmail) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findByLoginOrEmail(loginOrEmail);
        if (userEntity == null) {
            throw new UsernameNotFoundException("User not found, name or email invalid");
        }
        return new UserRepositoryUserDetails(userEntity);
    }

    private final static class UserRepositoryUserDetails extends UserEntity implements UserDetails {

        private UserRepositoryUserDetails (UserEntity userEntity) {
            super(userEntity);
        }

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities () {
            return Collections.singletonList(getRole());
        }

        @Override
        public String getUsername () {
            return this.getEmail();
        }

        @Override
        public boolean isAccountNonExpired () {
            return true;
        }

        @Override
        public boolean isAccountNonLocked () {
            return true;
        }

        @Override
        public boolean isCredentialsNonExpired () {
            return true;
        }

        @Override
        public boolean isEnabled () {
            return true;
        }

        @Override
        public String getPassword () {
            return super.getPassword();
        }
    }
}

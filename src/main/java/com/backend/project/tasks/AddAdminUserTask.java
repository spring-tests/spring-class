package com.backend.project.tasks;

import com.backend.project.model.entity.Role;
import com.backend.project.model.request.UserRequest;
import com.backend.project.model.response.UserResponse;
import com.backend.project.repository.RoleRepository;
import com.backend.project.service.UserService;
import java.util.List;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AddAdminUserTask implements Tasklet {

    @Autowired
    UserService userService;

    @Autowired
    RoleRepository roleRepository;

    @Override
    @SuppressWarnings("NullableProblems")
    public RepeatStatus execute (StepContribution stepContribution, ChunkContext chunkContext) {
        List<UserResponse> userEntities = userService.getAll();

        if (userEntities.isEmpty()) {
            Role role = new Role();

            role.setId(1L);
            role.setRole("ROLE_ADMIN");

            userService.addNewUser(
                    new UserRequest(
                            "Rafael",
                            "rafacg3343@gmail.com",
                            "senha123",
                            role));
        }

        return RepeatStatus.FINISHED;
    }
}
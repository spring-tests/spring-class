package com.backend.project.repository;


import com.backend.project.model.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    @Query("select u from USER_ENTITY u where u.name = ?1 or u.email = ?1")
    UserEntity findByLoginOrEmail (String loginOrEmail);
}

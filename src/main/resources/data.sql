insert into ROLES(id, role)
values
    (1, 'ROLE_ADMIN'),
    (2, 'ROLE_USER'),
    (3, 'ROLE_WATCHER')
ON CONFLICT DO NOTHING;

commit;
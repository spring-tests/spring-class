#
# Build Stage
#
FROM maven:3.5-jdk-8 AS build
COPY src docker/src
COPY pom.xml docker
RUN mvn -f docker/pom.xml clean package

#
# Package Stage
#
FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY --from=build docker/target/project.jar docker/project.jar
ENTRYPOINT [ "java", "-jar", "docker/project.jar" ]